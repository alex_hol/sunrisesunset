import React from 'react';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';

const GooglePlacesInput = ({updateLocation}) => {

  return (
    <GooglePlacesAutocomplete
      placeholder='Search'
      onPress={(data, details = null) => {
        updateLocation(data);

        console.log(data, details);
      }}
      query={{
        key: 'AIzaSyBLcEpqDcBdsaPQKkUpxI0ShiD6nhip64k',
        language: 'en',
      }}
      currentLocation={true}
      currentLocationLabel='Current location'
      enablePoweredByContainer={false}
      styles={{
        container: {
          flex: 1
        }
      }}
    />
  );
};

export default GooglePlacesInput;
