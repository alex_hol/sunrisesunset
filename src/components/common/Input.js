import React from 'react';
import {TextInput, StyleSheet, View} from 'react-native';

const Input = props => {
  return (
    <View style={styles.container}>
      <TextInput style={styles.input} {...props}/>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
  },
  input: {
    borderBottomColor: '#444',
    borderBottomWidth: 1,
    textAlign: 'center',
    fontSize: 22,
    color: '#fff',
    width: 300,
  },
});

export default Input;
