import React, {useState} from 'react';
import {SafeAreaView, StyleSheet, View, Text} from 'react-native';
import Button from '../common/Button';
import SunInfo from './SunInfo';
import GooglePlacesInput from '../common/GooglePlacesInput';

const HomeScreen = () => {
  const [location, setLocation] = useState('Location unknown');

  const updateLocation = data => setLocation(data);

  return (
    <SafeAreaView style={styles.container}>
      <Text style={{fontSize: 30, textAlign: 'center', marginBottom: 30}}>Sun</Text>
      <View style={styles.searchContainer}>
        <GooglePlacesInput updateLocation={updateLocation}/>
        <View style={{flex: 0.5, alignItems: 'center', paddingBottom: 60}}>
          <Button onPress={() => {}}>Search</Button>
          <Text style={styles.location}>{location}</Text>
        </View>
      </View>
      <SunInfo/>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  searchContainer: {
    backgroundColor: '#F0973D',
    height: 250,
    padding: 15,
  },
  location: {
    fontSize: 30,
    color: '#fff',
  },
});

export default HomeScreen;
