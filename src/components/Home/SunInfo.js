import React, {useEffect, useState, useLayoutEffect} from 'react';
import {SafeAreaView, StyleSheet, View, Text} from 'react-native';
import {getSunInfo} from '../../api/sun';
import {convertTime12to24, requestPermissions} from '../../utils';
import Geolocation from 'react-native-geolocation-service';

navigator.geolocation = require('react-native-geolocation-service');

const SunInfo = () => {
  const [sunInfo, setSunInfo] = useState({});
  const [currentPosition, setCurrentPosition] = useState({});

  useLayoutEffect(() => {
    requestPermissions()
      .then(() => Geolocation.getCurrentPosition(
        (position) => {
          setCurrentPosition(position.coords);
        },
        (error) => {
          console.log(error.code, error.message);
        },
        {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
        ),
      );
  }, []);

  useEffect(() => {
    getSunInfo(currentPosition.latitude, currentPosition.longitude)
      .then(res => {
        setSunInfo({
          ...res,
          sunrise: convertTime12to24(res.sunrise),
          sunset: convertTime12to24(res.sunset),
        });
      })
      .catch(error => console.log(`Api call error ${error.message}`));
  }, [currentPosition]);


  return (
    <SafeAreaView style={styles.container}>
      <View style={{padding: 10}}>
        <Text style={styles.text}>Sunrise {sunInfo.sunrise}🌅</Text>
        <Text style={styles.text}>Sunset  {sunInfo.sunset}🌇</Text>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 0.7,
    height: 50,
    margin: 15,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 25,
  },
});

export default SunInfo;
