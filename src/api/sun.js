import axios from 'axios';

const baseUrl = `https://api.sunrise-sunset.org/json`;

export const getSunInfo = (lat, lng) => {
  return axios.get(`${baseUrl}?lat=${lat}&lng=${lng}`)
    .then(({data}) => data.results)
    .catch(error => {
      throw error;
    });
}
